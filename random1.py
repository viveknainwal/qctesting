#import qiskit.circuit.random


#from qiskit import *
import qiskit
from qiskit import BasicAer
from qiskit import QuantumCircuit, execute
from qiskit.circuit.random import random_circuit
#from qiskit.visualization import plot_histogram
import matplotlib.pyplot as plt

circ = random_circuit(10, 5,max_operands=2, measure=True)
#circ.qasm(formatted=True,filename='random1.qasm')
#circ.draw(output='mpl')
sim_backend = qiskit.BasicAer.get_backend('dm_simulator')
qc1 = qiskit.execute(circ, backend=sim_backend)
#circ.draw(output='mpl')
#plt.show()
result = qc1.result()
print(result)

