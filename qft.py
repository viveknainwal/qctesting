
# coding: utf-8

# In[4]:

import numpy as np
# importing Qiskit
import qiskit
from qiskit import QuantumCircuit, execute
from qiskit.tools.monitor import job_monitor
import time
import matplotlib.pyplot as plt
import argparse
import sys
#from qiskit import Aer 

# In[5]:

from concurrent.futures import ThreadPoolExecutor
import resource

from time import sleep
#sys.setrecursionlimit(4000)


def qft_rotations(circuit, n):
    """Performs qft on the first n qubits in circuit (without swaps)"""
    if n == 0:
        return circuit
    n -= 1
    circuit.h(n)
    for qubit in range(n):
        circuit.cu1(np.pi/2**(n-qubit), qubit, n)
    # At the end of our function, we call the same function again on
    # the next qubits (we reduced n by one earlier in the function)
    qft_rotations(circuit, n)


def swap_registers(circuit, n):
    for qubit in range(n//2):
        circuit.swap(qubit, n-qubit-1)
    return circuit

def qft(circuit, n):
    """QFT on the first n qubits in circuit"""
    qft_rotations(circuit, n)
    swap_registers(circuit, n)
    return circuit


def elapsed_time(args,qu):
    backend = args.get('backend')
    shots = args.get('shots')
    sim_backend = qiskit.BasicAer.get_backend(backend)
#    sim_backend = Aer.get_backend(backend)
    qc = QuantumCircuit(qu)

    qft(qc,qu)
    start = time.time()
    options = {
        "thermal_factor": 0.,
        "decoherence_factor": .9,
        "depolarization_factor": 0.99,
        "bell_depolarization_factor": 0.99,
        "decay_factor": 0.99,
        "rotation_error": {'rx':[1., 0.], 'ry':[1., 0.], 'rz': [1., 0.]},
        "tsp_model_error": [1., 0.],
        "plot": False
    }
    qc1 = qiskit.execute(qc, backend=sim_backend, shots=shots, **options)
    result = qc1.result()
    #print(result)
    #qc.draw()

    elapsed = (time.time() - start)
       
    print(str(backend) + "," + str(qu) + "," + str(elapsed), flush=True)
    
    return elapsed

def plot_graph(lst1,lst2):
    plt.title('QFT on quantum circuit')
    plt.plot(lst2, lst1)
    plt.xticks(lst2)
    plt.yticks(lst1)
    plt.ylabel('Time elapsed in seconds')
    plt.xlabel('Number of Q Bits')
    plt.show()

    return True


# In[ ]:

def main():
#    args = {'backend':'qasm_simulator','shots':1024}
    args = {'backend':'dm_simulator','shots':1}
    print("Simulator,\t Number of Qbits,\t Time Taken")
    n_time = elapsed_time(args,10)



main()




